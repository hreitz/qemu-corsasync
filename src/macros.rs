/// Take an async function and export it as a coroutine_fn.
///
/// For certain return types, the exported function will have an added `errp` param:
/// - `Result<NonNull<$pointer>, $error>`: On success, the pointer is returned.  On error, `*errp`
///   is set, and `NULL` is returned.
/// - `std::io::Result<()>`: Exported function returns a `c_int`.  On success, 0 is returned.  On
///   error, `*errp` is set, and `-errno` is returned.
#[macro_export]
macro_rules! export_coroutine_fn {
    {
        async fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> Result<NonNull<$result_pointee_type:ty>, $error_type:ty>
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> Result<NonNull<$result_pointee_type>, $error_type> {
                $crate::co_run_async(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> std::io::Result<()>
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> std::io::Result<()> {
                $crate::co_run_async(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> $result_type:ty
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> $result_type {
                $crate::co_run_async(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        )
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) {
                $crate::co_run_async(async move $body)
            }
        }
    };
}

/// Take a normal function and export it for qemu.
///
/// For certain return types, the exported function will have an added `errp` param:
/// - `Result<NonNull<$pointer>, $error>`: On success, the pointer is returned.  On error, `*errp`
///   is set, and `NULL` is returned.
/// - `std::io::Result<()>`: Exported function returns a `c_int`.  On success, 0 is returned.  On
///   error, `*errp` is set, and `-errno` is returned.
#[macro_export]
macro_rules! export_fn {
    {
        fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> Result<NonNull<$result_pointee_type:ty>, $error_type:ty>
            $body:block
    } => {
        /// # Safety
        /// These are marked as unsafe to silence clippy about it.  These functions are only
        /// intended to be used from C code, which does not care much about the concept of safety
        /// anyway.
        #[no_mangle]
        pub unsafe extern "C" fn $fn_name(
            $($param_name: $param_type),*,
            errp: *mut *mut $crate::QemuError
        ) -> Option<NonNull<$result_pointee_type>> {
            $crate::with_errp(
                move || -> Result<NonNull<$result_pointee_type>, $error_type> {
                    $body
                },
                errp,
            )
        }
    };

    {
        fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> std::io::Result<()>
            $body:block
    } => {
        /// # Safety
        /// These are marked as unsafe to silence clippy about it.  These functions are only
        /// intended to be used from C code, which does not care much about the concept of safety
        /// anyway.
        #[no_mangle]
        pub unsafe extern "C" fn $fn_name(
            $($param_name: $param_type),*,
            errp: *mut *mut $crate::QemuError
        ) -> std::ffi::c_int {
            $crate::with_errp_to_int(
                || -> std::io::Result<()> {
                    $crate::co_run_async(async move $body)
                },
                errp
            )
        }
    };

    {
        fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> $result_type:ty
            $body:block
    } => {
        /// # Safety
        /// These are marked as unsafe to silence clippy about it.  These functions are only
        /// intended to be used from C code, which does not care much about the concept of safety
        /// anyway.
        #[no_mangle]
        pub unsafe extern "C" fn $fn_name(
            $($param_name: $param_type),*
        ) -> $result_type
            $body
    };

    {
        fn $fn_name:ident(
            $($param_name:ident: $param_type:ty),*
        )
            $body:block
    } => {
        /// # Safety
        /// These are marked as unsafe to silence clippy about it.  These functions are only
        /// intended to be used from C code, which does not care much about the concept of safety
        /// anyway.
        #[no_mangle]
        pub unsafe extern "C" fn $fn_name(
            $($param_name: $param_type),*
        )
            $body
    }
}

#[macro_export]
macro_rules! export_coroutine_fn_plus_mixed_wrapper {
    {
        async fn $fn_name:ident/$wrapper_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> Result<NonNull<$result_pointee_type:ty>, $error_type:ty>
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> Result<NonNull<$result_pointee_type>, $error_type> {
                $crate::co_run_async(async move $body)
            }
        }
        $crate::export_fn! {
            fn $wrapper_name($($param_name: $param_type),*) -> Result<NonNull<$result_pointee_type>, $error_type> {
                $crate::co_run_async_or_block(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident/$wrapper_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> std::io::Result<()>
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> std::io::Result<()> {
                $crate::co_run_async(async move $body)
            }
        }
        $crate::export_fn! {
            fn $wrapper_name($($param_name: $param_type),*) -> std::io::Result<()> {
                $crate::co_run_async_or_block(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident/$wrapper_name:ident(
            $($param_name:ident: $param_type:ty),*
        ) -> $result_type:ty
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) -> $result_type {
                $crate::co_run_async(async move $body)
            }
        }
        $crate::export_fn! {
            fn $wrapper_name($($param_name: $param_type),*) -> $result_type {
                $crate::co_run_async_or_block(async move $body)
            }
        }
    };

    {
        async fn $fn_name:ident/$wrapper_name:ident(
            $($param_name:ident: $param_type:ty),*
        )
            $body:block
    } => {
        $crate::export_fn! {
            fn $fn_name($($param_name: $param_type),*) {
                $crate::co_run_async(async move $body)
            }
        }
        $crate::export_fn! {
            fn $wrapper_name($($param_name: $param_type),*) {
                $crate::co_run_async_or_block(async move $body)
            }
        }
    };
}
