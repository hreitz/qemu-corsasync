use crate::qobject::error::Error;
use crate::strings::ToGMallocCString;
use std::ffi::{c_char, c_int, c_void, CStr};

macro_rules! c_enum {
    (
        $(#[$attr:meta])*
        $v:vis enum $enum_name:ident {
            $(
                $(#[$id_attr:meta])*
                $identifier:ident = $value:literal,
            )+
        }
    ) => {
        $(#[$attr])*
        #[derive(Copy, Clone, Debug, Eq, PartialEq)]
        #[repr(C)]
        $v enum $enum_name {
            $(
                $(#[$id_attr])*
                $identifier = $value,
            )+
        }

        impl TryFrom<std::ffi::c_int> for $enum_name {
            type Error = $crate::qobject::error::Error;
            fn try_from(val: std::ffi::c_int) -> Result<Self, $crate::qobject::error::Error> {
                match val {
                    $($value => Ok($enum_name::$identifier),)*
                    _ => Err(format!("Invalid value for {}: {:x}", stringify!($enum_name), val).into()),
                }
            }
        }
    }
}

extern "C" {
    fn g_free(ptr: *mut c_void);
    fn g_malloc0(size: usize) -> *mut c_void;
    fn strcmp(a: *const c_char, b: *const c_char) -> c_int;
    fn strlen(s: *const c_char) -> usize;

    static mut qnull_: QNull;
}

/// Typed (and thus "safe") version of `g_malloc0()`.  Only works for types whose size is greater
/// than 0.
/// Safety: The returned object will be initialized to byte-wise zero, i.e. may not be properly
/// initialized.
unsafe fn g_malloc0_typed<T: Sized>() -> &'static mut T {
    debug_assert!(std::mem::size_of::<T>() > 0);
    // Safe: g_malloc0() returns a valid pointer or panics
    unsafe { &mut *(g_malloc0(std::mem::size_of::<T>()) as *mut T) }
}

#[repr(C)]
pub struct QBool {
    pub base: QObjectBase_,
    pub value: bool,
}

#[repr(C)]
pub struct QDict {
    pub base: QObjectBase_,
    pub size: usize,
    pub table: [QemuListHead<QDictEntry>; 512],
}

#[repr(C)]
pub struct QDictEntry {
    pub key: *mut c_char,
    pub value: QObjectUnsafeRef<QObject>,
    pub next: QemuListEntry<QDictEntry>,
}

pub struct QDictIter<'a> {
    qdict: &'a QDict,
    next_bucket: usize,
    next_in_bucket: Option<&'a QDictEntry>,
}

#[repr(C)]
pub struct QList {
    pub base: QObjectBase_,
    pub head: QemuTailQLink<QListEntry>,
}

#[repr(C)]
pub struct QListEntry {
    pub value: QObjectUnsafeRef<QObject>,
    pub next: QemuTailQLink<QListEntry>,
}

pub struct QListIter<'a> {
    next: Option<&'a QListEntry>,
}

#[repr(C)]
pub struct QNull {
    pub base: QObjectBase_,
}

#[repr(C)]
pub struct QNum {
    pub base: QObjectBase_,
    pub kind: c_int,
    pub u: QNumValue,
}

c_enum! {
    pub enum QNumKind {
        I64 = 0,
        U64 = 1,
        Double = 2,
    }
}

pub enum QNumParsed {
    I64(i64),
    U64(u64),
    F64(f64),
}

#[repr(C)]
pub union QNumValue {
    pub i: i64,
    pub u: u64,
    pub dbl: f64,
}

#[repr(C)]
pub struct QObject {
    pub base: QObjectBase_,
}

#[repr(C)]
pub struct QObjectBase_ {
    pub type_: c_int,
    pub refcnt: usize,
}

/// Strong reference to a `QObject`.  May contain a NULL pointer to indicate no reference.
#[repr(C)]
pub struct QObjectRef<T: QObjectVariant>(*mut T);

// QObjects can be sent between threads (although they are not thread-safe)
unsafe impl<T: QObjectVariant> Send for QObjectRef<T> {}

/// Same as `QObjectRef`, but does not implement `Deref`; instead, use `.try_deref()` to check for
/// NULL pointers.  Should be used in FFI C structs instead of `QObject`, because we cannot trust
/// external parties to always put non-NULL pointers there.
#[repr(C)]
pub struct QObjectUnsafeRef<T: QObjectVariant>(*mut T);

// QObjects can be sent between threads (although they are not thread-safe)
unsafe impl<T: QObjectVariant> Send for QObjectUnsafeRef<T> {}

#[repr(C)]
pub struct QString {
    pub base: QObjectBase_,
    pub string: *const c_char,
}

c_enum! {
    pub enum QType {
        None = 0,
        QNull = 1,
        QNum = 2,
        QString = 3,
        QDict = 4,
        QList = 5,
        QBool = 6,
    }
}

#[repr(C)]
pub struct QemuListEntry<T> {
    pub le_next: *mut T,
    pub le_prev: *mut *mut T,
}

#[repr(C)]
pub struct QemuListHead<T> {
    pub lh_first: *mut T,
}

#[repr(C)]
pub struct QemuTailQLink<T> {
    pub tql_next: *mut T,
    pub tql_prev: *mut QemuTailQLink<T>,
}

pub trait QObjectVariant: Sized + 'static {
    const TYPE: QType;

    /// Safety: Only initializes the base correctly, but not the rest of the object (initialized to
    /// byte-wise zero).
    unsafe fn allocate() -> QObjectRef<Self> {
        debug_assert!(std::mem::size_of::<Self>() > 0);
        // Safe: Guaranteed by caller
        let obj = unsafe { g_malloc0_typed::<Self>() };
        let qobject = obj.as_mut_qobject();
        qobject.base.type_ = Self::TYPE as c_int;
        qobject.base.refcnt = 1;
        // Safe: We just set the refcount to 1
        unsafe { QObjectRef::new_unchecked(obj) }
    }

    fn try_from(obj: &QObject) -> Result<&Self, Error> {
        if obj.base.type_ == Self::TYPE as c_int {
            // Condition makes this safe
            Ok(unsafe { std::mem::transmute::<&QObject, &Self>(obj) })
        } else {
            Err(<Error as serde::de::Error>::invalid_type(
                obj.into(),
                &Self::TYPE,
            ))
        }
    }

    fn as_qobject(&self) -> &QObject {
        debug_assert!(std::mem::size_of::<Self>() >= std::mem::size_of::<QObject>());
        // `QObject` is the base type
        unsafe { std::mem::transmute::<&Self, &QObject>(self) }
    }

    fn as_mut_qobject(&mut self) -> &mut QObject {
        debug_assert!(std::mem::size_of::<Self>() >= std::mem::size_of::<QObject>());
        // `QObject` is the base type
        unsafe { std::mem::transmute::<&mut Self, &mut QObject>(self) }
    }
}

impl QBool {
    pub fn from_bool(v: bool) -> QObjectRef<QBool> {
        // Safe: We are going to set the value
        let mut qb = unsafe { Self::allocate() };
        qb.value = v;
        qb
    }
}

impl QObjectVariant for QBool {
    const TYPE: QType = QType::QBool;
}

impl QDict {
    pub fn new() -> QObjectRef<QDict> {
        // Safe: Having everything be byte-wise zero is correct
        unsafe { Self::allocate() }
    }

    pub fn iter(&self) -> QDictIter<'_> {
        QDictIter {
            qdict: self,
            next_bucket: 0,
            next_in_bucket: None,
        }
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    /// Taken from qemu's QDict implementation (qemu does not export this function, and we need it
    /// for API interoperability)
    fn tdb_hash(key: &[u8]) -> u32 {
        let mut value = 0x238f13afu32.wrapping_mul(key.len() as u32);
        for (i, b) in key.iter().enumerate() {
            value = value.wrapping_add((*b as u32) << ((i as u32).wrapping_mul(5) % 24));
        }
        1103515243u32.wrapping_mul(value).wrapping_add(12345)
    }

    pub fn push(&mut self, key: &str, value: QObjectRef<QObject>) -> Result<(), Error> {
        let bucket_index = Self::tdb_hash(key.as_bytes()) as usize % self.table.len();
        let mut entry_ptr = self.table[bucket_index].lh_first;
        while !entry_ptr.is_null() {
            // Safe: Guaranteed by convention
            let entry = unsafe { &mut *entry_ptr };

            if unsafe { strcmp(entry.key, key.as_bytes().as_ptr() as *const c_char) } == 0 {
                entry.value = value.into();
                return Ok(());
            }

            entry_ptr = entry.next.le_next;
        }

        // Safe: We are going to initialize what needs initialization
        let entry = unsafe { g_malloc0_typed::<QDictEntry>() };
        entry.key = key.to_gmalloc_cstring()?.as_ptr();
        entry.value = value.into();
        entry.next.le_next = self.table[bucket_index].lh_first;
        entry.next.le_prev = &mut self.table[bucket_index].lh_first as *mut *mut QDictEntry;

        self.table[bucket_index].lh_first = entry as *mut QDictEntry;
        self.size += 1;

        Ok(())
    }

    /// Safety: Caller must guarantee `key` is a valid C string that has been allocated with
    /// `g_malloc*()`
    pub unsafe fn push_with_cstr(&mut self, key: *mut c_char, value: QObjectRef<QObject>) {
        // Caller guarantees to be safe
        let len = unsafe { strlen(key) };
        // Caller guarantees to be safe
        let key_slice = unsafe { std::slice::from_raw_parts(key as *mut u8, len) };

        let bucket_index = Self::tdb_hash(key_slice) as usize % self.table.len();
        let mut entry_ptr = self.table[bucket_index].lh_first;
        while !entry_ptr.is_null() {
            // Safe: Guaranteed by convention
            let entry = unsafe { &mut *entry_ptr };

            if unsafe { strcmp(entry.key, key) } == 0 {
                entry.value = value.into();
                // Caller guarantees to be safe
                unsafe { g_free(key as *mut c_void) };
                return;
            }

            entry_ptr = entry.next.le_next;
        }

        // Safe: We are going to initialize what needs initialization.
        let entry = unsafe { g_malloc0_typed::<QDictEntry>() };
        entry.key = key;
        entry.value = value.into();
        entry.next.le_next = self.table[bucket_index].lh_first;
        entry.next.le_prev = &mut self.table[bucket_index].lh_first as *mut *mut QDictEntry;

        self.table[bucket_index].lh_first = entry as *mut QDictEntry;
        self.size += 1;
    }
}

impl QObjectVariant for QDict {
    const TYPE: QType = QType::QDict;
}

impl<'a> Iterator for QDictIter<'a> {
    type Item = Result<(&'a str, &'a QObject), Error>;

    fn next(&mut self) -> Option<Result<(&'a str, &'a QObject), Error>> {
        let next = if let Some(next) = self.next_in_bucket.take() {
            next
        } else {
            loop {
                if self.next_bucket >= self.qdict.table.len() {
                    return None;
                }
                let first_ptr = self.qdict.table[self.next_bucket].lh_first;
                if !first_ptr.is_null() {
                    // Safe by convention
                    break unsafe { &*first_ptr };
                }
                self.next_bucket += 1;
            }
        };

        let next_ptr = next.next.le_next;
        if !next_ptr.is_null() {
            // Safe by convention
            self.next_in_bucket = Some(unsafe { &*next_ptr });
        } else {
            self.next_bucket += 1;
        }

        // Safe by convention
        let key = match unsafe { CStr::from_ptr(next.key) }.to_str() {
            Ok(key) => key,
            Err(err) => return Some(Err(err.into())),
        };

        let value = match next.value.try_deref() {
            Ok(value) => value,
            Err(err) => return Some(Err(err.into())),
        };

        Some(Ok((key, value)))
    }
}

impl QList {
    pub fn new() -> QObjectRef<QList> {
        // Safe: We are going to initialize what needs initialization
        let mut ql = unsafe { Self::allocate() };
        ql.head.tql_prev = &mut ql.head;
        ql
    }

    pub fn iter(&self) -> QListIter<'_> {
        let first_ptr = self.head.tql_next;
        if !first_ptr.is_null() {
            // Safe by convention
            QListIter {
                next: Some(unsafe { &*first_ptr }),
            }
        } else {
            QListIter { next: None }
        }
    }

    pub fn push(&mut self, obj: QObjectRef<QObject>) {
        // Safe: Byte-wise zero representation is safe for QListEntry
        let qle = unsafe { g_malloc0_typed::<QListEntry>() };
        qle.value = obj.into();
        qle.next.tql_prev = self.head.tql_prev;

        // Safe by invariant (tql_prev is always valid)
        unsafe { (*self.head.tql_prev).tql_next = qle as *mut QListEntry };
        self.head.tql_prev = &mut qle.next as *mut QemuTailQLink<QListEntry>;
    }
}

impl QObjectVariant for QList {
    const TYPE: QType = QType::QList;
}

impl<'a> Iterator for QListIter<'a> {
    type Item = Result<&'a QObject, Error>;

    fn next(&mut self) -> Option<Result<&'a QObject, Error>> {
        if let Some(elm) = self.next.take() {
            let next_ptr = elm.next.tql_next;
            if !next_ptr.is_null() {
                // Safe by convention
                self.next = Some(unsafe { &*next_ptr });
            }
            Some(elm.value.try_deref())
        } else {
            None
        }
    }
}

impl QNull {
    pub fn new() -> QObjectRef<QNull> {
        // qemu requires `qnull_` to be the only instance of QNull
        // Not entirely safe (no atomic access), but qemu does this, too
        QObjectRef::new(unsafe { &mut qnull_ })
    }
}

impl QObjectVariant for QNull {
    const TYPE: QType = QType::QNull;
}

impl QNum {
    pub fn from_signed<T: Into<i64>>(v: T) -> QObjectRef<QNum> {
        // Safe: We are going to set the value
        let mut qn = unsafe { QNum::allocate() };
        qn.kind = QNumKind::I64 as c_int;
        // Safe because we set the discriminator accordingly
        qn.u.i = v.into();
        qn
    }

    pub fn from_unsigned<T: Into<u64>>(v: T) -> QObjectRef<QNum> {
        // Safe: We are going to set the value
        let mut qn = unsafe { QNum::allocate() };
        qn.kind = QNumKind::U64 as c_int;
        // Safe because we set the discriminator accordingly
        qn.u.u = v.into();
        qn
    }

    pub fn from_float<T: Into<f64>>(v: T) -> QObjectRef<QNum> {
        // Safe: We are going to set the value
        let mut qn = unsafe { QNum::allocate() };
        qn.kind = QNumKind::Double as c_int;
        qn.u.dbl = v.into();
        qn
    }
}

impl QObjectVariant for QNum {
    const TYPE: QType = QType::QNum;
}

impl TryFrom<&QNum> for QNumParsed {
    type Error = Error;

    fn try_from(qn: &QNum) -> Result<Self, Error> {
        // All union accesses here are safe because we check the discriminator
        Ok(match QNumKind::try_from(qn.kind)? {
            QNumKind::I64 => QNumParsed::I64(unsafe { qn.u.i }),
            QNumKind::U64 => QNumParsed::U64(unsafe { qn.u.u }),
            QNumKind::Double => QNumParsed::F64(unsafe { qn.u.dbl }),
        })
    }
}

impl QObject {
    pub fn try_into<V: QObjectVariant>(&self) -> Result<&V, Error> {
        V::try_from(self)
    }
}

impl QObjectVariant for QObject {
    const TYPE: QType = QType::None;
}

impl<'a> From<&'a QObject> for serde::de::Unexpected<'a> {
    fn from(obj: &'a QObject) -> serde::de::Unexpected<'a> {
        use serde::de::Unexpected;

        match QType::try_from(obj.base.type_) {
            Ok(QType::None) => Unexpected::Other("no type"),
            Ok(QType::QNull) => Unexpected::Unit,
            Ok(QType::QNum) => {
                // Must succeed, we just checked the type
                let qn: &QNum = obj.try_into().unwrap();
                match QNumParsed::try_from(qn) {
                    Ok(QNumParsed::I64(i)) => Unexpected::Signed(i),
                    Ok(QNumParsed::U64(u)) => Unexpected::Unsigned(u),
                    Ok(QNumParsed::F64(f)) => Unexpected::Float(f),
                    _ => Unexpected::Other("unknown number type"),
                }
            }
            Ok(QType::QString) => {
                // Must succeed, we just checked the type
                let qs: &QString = obj.try_into().unwrap();
                Unexpected::Str(qs.try_into().unwrap_or("<invalid UTF-8>"))
            }
            Ok(QType::QDict) => Unexpected::Map,
            Ok(QType::QList) => Unexpected::Seq,
            Ok(QType::QBool) => {
                // Must succeed, we just checked the type
                let qb: &QBool = obj.try_into().unwrap();
                Unexpected::Bool(qb.value)
            }
            Err(_) => Unexpected::Other("unknown type"),
        }
    }
}

impl<T: QObjectVariant> QObjectRef<T> {
    /// Safety: Does not increment `obj`'s refcount, i.e. the caller must ensure that `obj` is a
    /// strong reference already.
    unsafe fn new_unchecked(obj: &mut T) -> Self {
        QObjectRef(obj as *mut T)
    }

    fn new(obj: &mut T) -> Self {
        obj.as_mut_qobject().base.refcnt += 1;
        // Safe: We just incremented the refcount
        unsafe { Self::new_unchecked(obj) }
    }

    pub fn into_qobject(self) -> QObjectRef<QObject> {
        // Safe: These pointers have the same internal representation, and `QObject` is the base
        // object of any `QObjectVariant`
        unsafe { std::mem::transmute::<QObjectRef<T>, QObjectRef<QObject>>(self) }
    }

    /// Return the contained reference, leaking the strong refcount
    pub fn leak(mut self) -> &'static mut T {
        if self.0.is_null() {
            panic!("QObject reference is empty");
        }
        let r = std::mem::replace(&mut self.0, std::ptr::null_mut());
        // Safe: This is a strong reference
        unsafe { &mut *r }
    }
}

impl<T: QObjectVariant> Drop for QObjectRef<T> {
    fn drop(&mut self) {
        if self.0.is_null() {
            return;
        }

        let base = &mut self.as_mut_qobject().base;
        base.refcnt -= 1;
        if base.refcnt == 0 {
            // Safe:
            // - The refcount is 0, so this is no longer used.
            // - This was allocated with g_malloc*().
            unsafe { g_free(self.0 as *mut c_void) };
        }
    }
}

impl<T: QObjectVariant> std::ops::Deref for QObjectRef<T> {
    type Target = T;

    fn deref(&self) -> &T {
        if self.0.is_null() {
            panic!("QObject reference is empty");
        }
        // Safe: We have a strong reference
        unsafe { &*self.0 }
    }
}

impl<T: QObjectVariant> std::ops::DerefMut for QObjectRef<T> {
    fn deref_mut(&mut self) -> &mut T {
        if self.0.is_null() {
            panic!("QObject reference is empty");
        }
        // Safe: We have a strong reference
        unsafe { &mut *self.0 }
    }
}

impl<T: QObjectVariant> TryFrom<QObjectUnsafeRef<T>> for QObjectRef<T> {
    type Error = Error;

    fn try_from(ur: QObjectUnsafeRef<T>) -> Result<Self, Error> {
        if ur.0.is_null() {
            return Err("QObject reference is empty".into());
        }
        // Safe: Both have the same representation
        Ok(unsafe { std::mem::transmute::<QObjectUnsafeRef<T>, QObjectRef<T>>(ur) })
    }
}

impl<T: QObjectVariant> QObjectUnsafeRef<T> {
    fn try_deref(&self) -> Result<&T, Error> {
        if self.0.is_null() {
            return Err("QObject reference is empty".into());
        }
        // Safe: We have a strong reference
        Ok(unsafe { &*self.0 })
    }
}

impl<T: QObjectVariant> From<QObjectRef<T>> for QObjectUnsafeRef<T> {
    fn from(sr: QObjectRef<T>) -> Self {
        // Safe: Both have the same representation
        unsafe { std::mem::transmute::<QObjectRef<T>, QObjectUnsafeRef<T>>(sr) }
    }
}

impl QString {
    pub fn from_str(s: &str) -> Result<QObjectRef<QString>, Error> {
        // Safe: We are going to set the value
        let mut qs = unsafe { QString::allocate() };
        qs.string = s.to_gmalloc_cstring()?.as_ptr();
        Ok(qs)
    }
}

impl QObjectVariant for QString {
    const TYPE: QType = QType::QString;
}

impl<'a> TryFrom<&'a QString> for &'a str {
    type Error = std::str::Utf8Error;

    fn try_from(qs: &'a QString) -> Result<&'a str, Self::Error> {
        // Who created this object guarantees this is safe
        unsafe { CStr::from_ptr(qs.string) }.to_str()
    }
}

impl serde::de::Expected for QType {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Debug>::fmt(self, formatter)
    }
}
