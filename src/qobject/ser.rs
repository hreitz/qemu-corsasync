use crate::qobject::error::Error;
use crate::qobject::structs::*;
use std::ffi::c_char;
use std::ptr::NonNull;

mod helpers;

pub struct Serializer;

pub struct QListSerializer(QObjectRef<QList>);

pub struct QDictSerializer {
    dict: QObjectRef<QDict>,
    current_key: Option<NonNull<c_char>>,
}

pub struct TaggedSerializer<T> {
    key: &'static str,
    inner: T,
}

impl Serializer {
    pub fn new() -> Self {
        Serializer {}
    }
}

impl serde::ser::Serializer for Serializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    type SerializeSeq = QListSerializer;
    type SerializeTuple = QListSerializer;
    type SerializeTupleStruct = QListSerializer;
    type SerializeTupleVariant = TaggedSerializer<QListSerializer>;
    type SerializeMap = QDictSerializer;
    type SerializeStruct = QDictSerializer;
    type SerializeStructVariant = TaggedSerializer<QDictSerializer>;

    fn serialize_bool(self, v: bool) -> Result<Self::Ok, Error> {
        Ok(QBool::from_bool(v).into_qobject())
    }

    fn serialize_i8(self, v: i8) -> Result<Self::Ok, Error> {
        Ok(QNum::from_signed(v).into_qobject())
    }

    fn serialize_i16(self, v: i16) -> Result<Self::Ok, Error> {
        Ok(QNum::from_signed(v).into_qobject())
    }

    fn serialize_i32(self, v: i32) -> Result<Self::Ok, Error> {
        Ok(QNum::from_signed(v).into_qobject())
    }

    fn serialize_i64(self, v: i64) -> Result<Self::Ok, Error> {
        Ok(QNum::from_signed(v).into_qobject())
    }

    fn serialize_u8(self, v: u8) -> Result<Self::Ok, Error> {
        Ok(QNum::from_unsigned(v).into_qobject())
    }

    fn serialize_u16(self, v: u16) -> Result<Self::Ok, Error> {
        Ok(QNum::from_unsigned(v).into_qobject())
    }

    fn serialize_u32(self, v: u32) -> Result<Self::Ok, Error> {
        Ok(QNum::from_unsigned(v).into_qobject())
    }

    fn serialize_u64(self, v: u64) -> Result<Self::Ok, Error> {
        Ok(QNum::from_unsigned(v).into_qobject())
    }

    fn serialize_f32(self, v: f32) -> Result<Self::Ok, Error> {
        Ok(QNum::from_float(v).into_qobject())
    }

    fn serialize_f64(self, v: f64) -> Result<Self::Ok, Error> {
        Ok(QNum::from_float(v).into_qobject())
    }

    fn serialize_char(self, v: char) -> Result<Self::Ok, Error> {
        Ok(QString::from_str(&v.to_string())?.into_qobject())
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok, Error> {
        Ok(QString::from_str(v)?.into_qobject())
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Error> {
        let mut ql = QList::new();
        for x in v {
            ql.push(QNum::from_unsigned(*x).into_qobject());
        }
        Ok(ql.into_qobject())
    }

    fn serialize_none(self) -> Result<Self::Ok, Error> {
        Ok(QNull::new().into_qobject())
    }

    fn serialize_some<T: serde::ser::Serialize + ?Sized>(self, v: &T) -> Result<Self::Ok, Error> {
        v.serialize(self)
    }

    fn serialize_unit(self) -> Result<Self::Ok, Error> {
        // serde_json represents `()` as null, but qemu uses an empty dict instead
        Ok(QDict::new().into_qobject())
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Error> {
        self.serialize_unit()
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
    ) -> Result<Self::Ok, Error> {
        self.serialize_str(variant)
    }

    fn serialize_newtype_struct<T: serde::ser::Serialize + ?Sized>(
        self,
        _name: &'static str,
        v: &T,
    ) -> Result<Self::Ok, Error> {
        v.serialize(self)
    }

    fn serialize_newtype_variant<T: serde::ser::Serialize + ?Sized>(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        v: &T,
    ) -> Result<Self::Ok, Error> {
        let mut qd = QDict::new();
        qd.push(variant, v.serialize(self)?)?;
        Ok(qd.into_qobject())
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<QListSerializer, Error> {
        Ok(QListSerializer::new())
    }

    fn serialize_tuple(self, _len: usize) -> Result<QListSerializer, Error> {
        Ok(QListSerializer::new())
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<QListSerializer, Error> {
        Ok(QListSerializer::new())
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<TaggedSerializer<QListSerializer>, Error> {
        Ok(TaggedSerializer::new(variant, QListSerializer::new()))
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<QDictSerializer, Error> {
        Ok(QDictSerializer::new())
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<QDictSerializer, Error> {
        Ok(QDictSerializer::new())
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<TaggedSerializer<QDictSerializer>, Error> {
        Ok(TaggedSerializer::new(variant, QDictSerializer::new()))
    }
}

impl QDictSerializer {
    fn new() -> Self {
        QDictSerializer {
            dict: QDict::new(),
            current_key: None,
        }
    }
}

impl serde::ser::SerializeMap for QDictSerializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_key<T: serde::ser::Serialize + ?Sized>(&mut self, key: &T) -> Result<(), Error> {
        let key = key.serialize(helpers::CStrSerializer::new())?;
        self.current_key = Some(key);
        Ok(())
    }

    fn serialize_value<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        value: &T,
    ) -> Result<(), Error> {
        let key = self.current_key.take().unwrap();
        let value = value.serialize(Serializer::new())?;
        // Safe: `CStrSerializer` guarantees this is a valid C string allocated with `g_malloc*()`
        unsafe { self.dict.push_with_cstr(key.as_ptr(), value) };
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Error> {
        Ok(self.dict.into_qobject())
    }
}

impl serde::ser::SerializeStruct for QDictSerializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_field<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        key: &'static str,
        value: &T,
    ) -> Result<(), Error> {
        let value = value.serialize(Serializer::new())?;
        self.dict.push(key, value)?;
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Error> {
        Ok(self.dict.into_qobject())
    }
}

impl QListSerializer {
    fn new() -> Self {
        QListSerializer(QList::new())
    }
}

impl serde::ser::SerializeSeq for QListSerializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_element<T: serde::ser::Serialize + ?Sized>(&mut self, v: &T) -> Result<(), Error> {
        Ok(self.0.push(v.serialize(Serializer::new())?))
    }

    fn end(self) -> Result<Self::Ok, Error> {
        Ok(self.0.into_qobject())
    }
}

impl serde::ser::SerializeTuple for QListSerializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_element<T: serde::ser::Serialize + ?Sized>(&mut self, v: &T) -> Result<(), Error> {
        Ok(self.0.push(v.serialize(Serializer::new())?))
    }

    fn end(self) -> Result<Self::Ok, Error> {
        Ok(self.0.into_qobject())
    }
}

impl serde::ser::SerializeTupleStruct for QListSerializer {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_field<T: serde::ser::Serialize + ?Sized>(&mut self, v: &T) -> Result<(), Error> {
        Ok(self.0.push(v.serialize(Serializer::new())?))
    }

    fn end(self) -> Result<Self::Ok, Error> {
        Ok(self.0.into_qobject())
    }
}

impl<T> TaggedSerializer<T> {
    fn new(key: &'static str, inner: T) -> Self {
        TaggedSerializer { key, inner }
    }
}

impl serde::ser::SerializeStructVariant for TaggedSerializer<QDictSerializer> {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_field<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        key: &'static str,
        value: &T,
    ) -> Result<(), Error> {
        let value = value.serialize(Serializer::new())?;
        self.inner.dict.push(key, value)?;
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Error> {
        let mut qd = QDict::new();
        qd.push(self.key, self.inner.dict.into_qobject())?;
        Ok(qd.into_qobject())
    }
}

impl serde::ser::SerializeTupleVariant for TaggedSerializer<QListSerializer> {
    type Ok = QObjectRef<QObject>;
    type Error = Error;

    fn serialize_field<T: serde::ser::Serialize + ?Sized>(&mut self, v: &T) -> Result<(), Error> {
        Ok(self.inner.0.push(v.serialize(Serializer::new())?))
    }

    fn end(self) -> Result<Self::Ok, Error> {
        let mut qd = QDict::new();
        qd.push(self.key, self.inner.0.into_qobject())?;
        Ok(qd.into_qobject())
    }
}

pub fn to_qobject<T: serde::ser::Serialize>(obj: &T) -> Result<QObjectRef<QObject>, Error> {
    obj.serialize(Serializer::new())
}
