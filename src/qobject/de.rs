use crate::qobject::error::Error;
use crate::qobject::structs::*;
use std::ffi::c_int;

pub struct Deserializer<'de> {
    obj: &'de QObject,
    context: DeserializerContext,
}

#[derive(Clone)]
pub struct DeserializerContext {
    /// Whether legacy qemu conversions shall be applied (e.g. string `"on"` to bool `true`)
    legacy: bool,
}

pub struct QDictEnumAccess<'a, 'de> {
    iter: QDictIter<'de>,
    context: &'a DeserializerContext,
}

pub struct QDictEnumVariantAccess<'de> {
    value: &'de QObject,
    context: DeserializerContext,
}

pub struct QDictMapAccess<'a, 'de> {
    iter: QDictIter<'de>,
    next_value: Option<&'de QObject>,
    context: &'a DeserializerContext,
}

pub struct QListSeqAccess<'a, 'de> {
    iter: QListIter<'de>,
    context: &'a DeserializerContext,
}

impl<'de> Deserializer<'de> {
    pub fn from_qobject(input: &'de QObject, context: DeserializerContext) -> Self {
        Deserializer {
            obj: input,
            context,
        }
    }
}

impl<'a, 'de> QDictEnumAccess<'a, 'de> {
    fn new(dict: &'de QDict, context: &'a DeserializerContext) -> Self {
        QDictEnumAccess {
            iter: dict.iter(),
            context,
        }
    }
}

impl<'a, 'de> serde::de::EnumAccess<'de> for QDictEnumAccess<'a, 'de> {
    type Error = Error;
    type Variant = QDictEnumVariantAccess<'de>;

    fn variant_seed<V: serde::de::DeserializeSeed<'de>>(
        mut self,
        seed: V,
    ) -> Result<(V::Value, Self::Variant), Error> {
        // TODO: As in `Deserializer::deserialize_enum()`, this is just taken from serde's JSON
        // example
        if let Some(result) = self.iter.next() {
            let (key, value) = result?;

            if self.iter.next().is_some() {
                return Err("Superfluous fields after enum".into());
            }

            let variant = QDictEnumVariantAccess {
                value,
                context: self.context.clone(),
            };
            let value = seed.deserialize(serde::de::value::StrDeserializer::<Error>::new(key))?;
            Ok((value, variant))
        } else {
            Err(<Error as serde::de::Error>::missing_field("enum tag"))
        }
    }
}

impl<'de> serde::de::VariantAccess<'de> for QDictEnumVariantAccess<'de> {
    // TODO: As in `Deserializer::deserialize_enum()`, this is just taken from serde's JSON
    // example

    type Error = Error;

    fn unit_variant(self) -> Result<(), Error> {
        // Should have been handled as a pure string by `deserialize_enum()`
        Err(<Error as serde::de::Error>::invalid_type(
            self.value.into(),
            &"nothing",
        ))
    }

    fn newtype_variant_seed<T: serde::de::DeserializeSeed<'de>>(
        self,
        seed: T,
    ) -> Result<T::Value, Error> {
        seed.deserialize(Deserializer::from_qobject(self.value, self.context))
    }

    fn tuple_variant<V: serde::de::Visitor<'de>>(
        self,
        _len: usize,
        visitor: V,
    ) -> Result<V::Value, Error> {
        let de = Deserializer::from_qobject(self.value, self.context);
        serde::de::Deserializer::deserialize_seq(de, visitor)
    }

    fn struct_variant<V: serde::de::Visitor<'de>>(
        self,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Error> {
        let de = Deserializer::from_qobject(self.value, self.context);
        serde::de::Deserializer::deserialize_map(de, visitor)
    }
}

impl<'a, 'de> QDictMapAccess<'a, 'de> {
    fn new(dict: &'de QDict, context: &'a DeserializerContext) -> Self {
        QDictMapAccess {
            iter: dict.iter(),
            next_value: None,
            context,
        }
    }
}

impl<'a, 'de> serde::de::MapAccess<'de> for QDictMapAccess<'a, 'de> {
    type Error = Error;

    fn next_key_seed<K: serde::de::DeserializeSeed<'de>>(
        &mut self,
        seed: K,
    ) -> Result<Option<K::Value>, Error> {
        if let Some(result) = self.iter.next() {
            let (key, value) = result?;
            self.next_value = Some(value);
            Ok(Some(seed.deserialize(
                serde::de::value::StrDeserializer::<Error>::new(key),
            )?))
        } else {
            Ok(None)
        }
    }

    fn next_value_seed<V: serde::de::DeserializeSeed<'de>>(
        &mut self,
        seed: V,
    ) -> Result<V::Value, Error> {
        let value = self.next_value.take().unwrap();
        let value = seed.deserialize(Deserializer::from_qobject(value, self.context.clone()))?;
        Ok(value)
    }
}

impl<'a, 'de> QListSeqAccess<'a, 'de> {
    fn new(list: &'de QList, context: &'a DeserializerContext) -> Self {
        QListSeqAccess {
            iter: list.iter(),
            context,
        }
    }
}

impl<'a, 'de> serde::de::SeqAccess<'de> for QListSeqAccess<'a, 'de> {
    type Error = Error;

    fn next_element_seed<T: serde::de::DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<Option<T::Value>, Error> {
        if let Some(qobj) = self.iter.next() {
            let val = seed.deserialize(Deserializer::from_qobject(qobj?, self.context.clone()))?;
            Ok(Some(val))
        } else {
            Ok(None)
        }
    }
}

macro_rules! float_to_int_checked {
    ($into:ty, $value:expr) => {
        // TODO: Bounds check is probably not quite correct for i64/u64, check it
        if $value.fract() == 0.0 && $value >= <$into>::MIN as f64 && $value <= <$into>::MAX as f64 {
            Ok($value.trunc() as $into)
        } else {
            Err(format!(
                "Cannot convert {:?} into {}",
                $value,
                stringify!($into)
            ))
        }
    };
}

impl<'de> serde::de::Deserializer<'de> for Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        match QType::try_from(self.obj.base.type_) {
            Ok(QType::None) => Err(<Error as serde::de::Error>::invalid_type(
                self.obj.into(),
                &visitor,
            )),
            Ok(QType::QNull) => self.deserialize_unit(visitor),
            Ok(QType::QNum) => {
                // Must succeed, we just checked the type
                match QNumKind::try_from(self.obj.try_into::<QNum>().unwrap().kind) {
                    Ok(QNumKind::I64) => self.deserialize_i64(visitor),
                    Ok(QNumKind::U64) => self.deserialize_u64(visitor),
                    Ok(QNumKind::Double) => self.deserialize_f64(visitor),
                    _ => Err(<Error as serde::de::Error>::invalid_type(
                        self.obj.into(),
                        &visitor,
                    )),
                }
            }
            Ok(QType::QString) => self.deserialize_str(visitor),
            Ok(QType::QDict) => self.deserialize_map(visitor),
            Ok(QType::QList) => self.deserialize_seq(visitor),
            Ok(QType::QBool) => self.deserialize_bool(visitor),
            Err(_) => Err(<Error as serde::de::Error>::invalid_type(
                self.obj.into(),
                &visitor,
            )),
        }
    }

    fn deserialize_bool<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        if self.context.legacy && self.obj.base.type_ == QType::QString as c_int {
            // Must succeed, we just checked the type
            let qs: &QString = self.obj.try_into().unwrap();
            let s: &str = qs.try_into()?;
            return match s {
                "on" => visitor.visit_bool(true),
                "off" => visitor.visit_bool(false),
                _ => Err(<Error as serde::de::Error>::invalid_value(
                    self.obj.into(),
                    &"'on' or 'off'",
                )),
            };
        }

        let qb: &QBool = self.obj.try_into()?;
        visitor.visit_bool(qb.value)
    }

    fn deserialize_i8<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(i8, f)?,
        };
        visitor.visit_i8(val)
    }

    fn deserialize_i16<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(i16, f)?,
        };
        visitor.visit_i16(val)
    }

    fn deserialize_i32<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(i32, f)?,
        };
        visitor.visit_i32(val)
    }

    fn deserialize_i64<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(i64, f)?,
        };
        visitor.visit_i64(val)
    }

    fn deserialize_u8<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(u8, f)?,
        };
        visitor.visit_u8(val)
    }

    fn deserialize_u16<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(u16, f)?,
        };
        visitor.visit_u16(val)
    }

    fn deserialize_u32<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u.try_into()?,
            QNumParsed::F64(f) => float_to_int_checked!(u32, f)?,
        };
        visitor.visit_u32(val)
    }

    fn deserialize_u64<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i.try_into()?,
            QNumParsed::U64(u) => u,
            QNumParsed::F64(f) => float_to_int_checked!(u64, f)?,
        };
        visitor.visit_u64(val)
    }

    fn deserialize_f32<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        // Ignore precision for float targets
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i as f32,
            QNumParsed::U64(u) => u as f32,
            QNumParsed::F64(f) => f as f32,
        };
        visitor.visit_f32(val)
    }

    fn deserialize_f64<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qn: &QNum = self.obj.try_into()?;
        // Ignore precision for float targets
        let val = match QNumParsed::try_from(qn)? {
            QNumParsed::I64(i) => i as f64,
            QNumParsed::U64(u) => u as f64,
            QNumParsed::F64(f) => f,
        };
        visitor.visit_f64(val)
    }

    fn deserialize_char<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qs: &QString = self.obj.try_into()?;
        let s: &str = qs.try_into()?;
        let mut chars = s.chars();
        let Some(c) = chars.next() else {
            return Err("Expected single character, found empty string".into());
        };
        if chars.next().is_some() {
            return Err(format!(
                "Expected single character, found multi-character string: {}",
                s
            )
            .into());
        }
        visitor.visit_char(c)
    }

    fn deserialize_str<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qs: &QString = self.obj.try_into()?;
        visitor.visit_borrowed_str(qs.try_into()?)
    }

    fn deserialize_string<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        self.deserialize_str(visitor)
    }

    fn deserialize_bytes<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        self.deserialize_byte_buf(visitor)
    }

    fn deserialize_byte_buf<V: serde::de::Visitor<'de>>(
        self,
        visitor: V,
    ) -> Result<V::Value, Error> {
        let ql: &QList = self.obj.try_into()?;
        let mut bytes = Vec::<u8>::new();
        for e in ql.iter() {
            let qn: &QNum = e?.try_into()?;
            let val: u8 = match QNumParsed::try_from(qn)? {
                QNumParsed::I64(i) => i.try_into()?,
                QNumParsed::U64(u) => u.try_into()?,
                QNumParsed::F64(f) => float_to_int_checked!(u8, f)?,
            };
            bytes.push(val);
        }
        visitor.visit_byte_buf(bytes)
    }

    fn deserialize_option<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        if self.obj.base.type_ == QType::QNull as c_int {
            visitor.visit_none()
        } else {
            visitor.visit_some(self)
        }
    }

    fn deserialize_unit<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        // serde_json represents `()` as null, but qemu uses an empty dict instead
        let qd: &QDict = self.obj.try_into()?;
        if qd.is_empty() {
            visitor.visit_unit()
        } else {
            Err(<Error as serde::de::Error>::invalid_type(
                self.obj.into(),
                &"empty map",
            ))
        }
    }

    fn deserialize_unit_struct<V: serde::de::Visitor<'de>>(
        self,
        _name: &'static str,
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_unit(visitor)
    }

    fn deserialize_newtype_struct<V: serde::de::Visitor<'de>>(
        self,
        _name: &'static str,
        visitor: V,
    ) -> Result<V::Value, Error> {
        visitor.visit_newtype_struct(self)
    }

    fn deserialize_seq<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let ql: &QList = self.obj.try_into()?;
        visitor.visit_seq(QListSeqAccess::new(ql, &self.context))
    }

    fn deserialize_tuple<V: serde::de::Visitor<'de>>(
        self,
        _len: usize,
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_seq(visitor)
    }

    fn deserialize_tuple_struct<V: serde::de::Visitor<'de>>(
        self,
        _name: &'static str,
        _len: usize,
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_seq(visitor)
    }

    fn deserialize_map<V: serde::de::Visitor<'de>>(self, visitor: V) -> Result<V::Value, Error> {
        let qd: &QDict = self.obj.try_into()?;
        visitor.visit_map(QDictMapAccess::new(qd, &self.context))
    }

    fn deserialize_struct<V: serde::de::Visitor<'de>>(
        self,
        _name: &'static str,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_map(visitor)
    }

    fn deserialize_enum<V: serde::de::Visitor<'de>>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Error> {
        // TODO: No real idea what serde wants here, following the JSON tutorial (QObject is
        // basically binary JSON after all)
        match QType::try_from(self.obj.base.type_) {
            Ok(QType::QString) => {
                // Must succeed, we just checked the type
                let qs: &QString = self.obj.try_into().unwrap();
                visitor.visit_enum(serde::de::value::StrDeserializer::<Error>::new(
                    qs.try_into()?,
                ))
            }
            Ok(QType::QDict) => {
                // Must succeed, we just checked the type
                let qd: &QDict = self.obj.try_into().unwrap();
                visitor.visit_enum(QDictEnumAccess::new(qd, &self.context))
            }
            _ => Err(<Error as serde::de::Error>::invalid_type(
                self.obj.into(),
                &visitor,
            )),
        }
    }

    fn deserialize_identifier<V: serde::de::Visitor<'de>>(
        self,
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_str(visitor)
    }

    fn deserialize_ignored_any<V: serde::de::Visitor<'de>>(
        self,
        visitor: V,
    ) -> Result<V::Value, Error> {
        self.deserialize_any(visitor)
    }
}

pub fn from_qobject<'a, T: serde::de::Deserialize<'a>>(qobject: &'a QObject) -> Result<T, Error> {
    let ctx = DeserializerContext { legacy: false };
    let de = Deserializer::from_qobject(qobject, ctx);
    serde::de::Deserialize::deserialize(de)
}

pub fn from_qobject_legacy<'a, T: serde::de::Deserialize<'a>>(
    qobject: &'a QObject,
) -> Result<T, Error> {
    let ctx = DeserializerContext { legacy: true };
    let de = Deserializer::from_qobject(qobject, ctx);
    serde::de::Deserialize::deserialize(de)
}
