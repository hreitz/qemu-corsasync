use crate::qobject::error::Error;
use crate::strings::ToGMallocCString;
use std::ffi::c_char;

/// Serializer that only serializes strings, and returns a `g_malloc()`-ed C string
pub struct CStrSerializer;

impl CStrSerializer {
    pub fn new() -> Self {
        CStrSerializer {}
    }
}

impl serde::ser::Serializer for CStrSerializer {
    type Ok = std::ptr::NonNull<c_char>;
    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Bool(v),
            &"string",
        ))
    }

    fn serialize_i8(self, v: i8) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Signed(v.into()),
            &"string",
        ))
    }

    fn serialize_i16(self, v: i16) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Signed(v.into()),
            &"string",
        ))
    }

    fn serialize_i32(self, v: i32) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Signed(v.into()),
            &"string",
        ))
    }

    fn serialize_i64(self, v: i64) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Signed(v),
            &"string",
        ))
    }

    fn serialize_u8(self, v: u8) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unsigned(v.into()),
            &"string",
        ))
    }

    fn serialize_u16(self, v: u16) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unsigned(v.into()),
            &"string",
        ))
    }

    fn serialize_u32(self, v: u32) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unsigned(v.into()),
            &"string",
        ))
    }

    fn serialize_u64(self, v: u64) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unsigned(v),
            &"string",
        ))
    }

    fn serialize_f32(self, v: f32) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Float(v.into()),
            &"string",
        ))
    }

    fn serialize_f64(self, v: f64) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Float(v),
            &"string",
        ))
    }

    fn serialize_char(self, v: char) -> Result<Self::Ok, Error> {
        Ok(v.to_string().to_gmalloc_cstring()?)
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok, Error> {
        Ok(v.to_gmalloc_cstring()?)
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Bytes(v),
            &"string",
        ))
    }

    fn serialize_none(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Option,
            &"string",
        ))
    }

    fn serialize_some<T: serde::ser::Serialize + ?Sized>(self, _v: &T) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Option,
            &"string",
        ))
    }

    fn serialize_unit(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unit,
            &"string",
        ))
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Unit,
            &"string",
        ))
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
    ) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::UnitVariant,
            &"string",
        ))
    }

    fn serialize_newtype_struct<T: serde::ser::Serialize + ?Sized>(
        self,
        _name: &'static str,
        _v: &T,
    ) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::NewtypeStruct,
            &"string",
        ))
    }

    fn serialize_newtype_variant<T: serde::ser::Serialize + ?Sized>(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _v: &T,
    ) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::NewtypeVariant,
            &"string",
        ))
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }

    fn serialize_tuple_struct(self, _name: &'static str, _len: usize) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::TupleVariant,
            &"string",
        ))
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::StructVariant,
            &"string",
        ))
    }
}

impl serde::ser::SerializeMap for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_key<T: serde::ser::Serialize + ?Sized>(&mut self, _key: &T) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }
    fn serialize_value<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        _value: &T,
    ) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }
}

impl serde::ser::SerializeSeq for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_element<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        _v: &T,
    ) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
}

impl serde::ser::SerializeStruct for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_field<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        _key: &'static str,
        _value: &T,
    ) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Map,
            &"string",
        ))
    }
}

impl serde::ser::SerializeStructVariant for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_field<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        _key: &'static str,
        _value: &T,
    ) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::StructVariant,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::StructVariant,
            &"string",
        ))
    }
}

impl serde::ser::SerializeTuple for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_element<T: serde::ser::Serialize + ?Sized>(
        &mut self,
        _v: &T,
    ) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
}

impl serde::ser::SerializeTupleStruct for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_field<T: serde::ser::Serialize + ?Sized>(&mut self, _v: &T) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::Seq,
            &"string",
        ))
    }
}

impl serde::ser::SerializeTupleVariant for CStrSerializer {
    type Ok = <Self as serde::ser::Serializer>::Ok;
    type Error = <Self as serde::ser::Serializer>::Error;
    fn serialize_field<T: serde::ser::Serialize + ?Sized>(&mut self, _v: &T) -> Result<(), Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::TupleVariant,
            &"string",
        ))
    }
    fn end(self) -> Result<Self::Ok, Error> {
        Err(<Error as serde::de::Error>::invalid_type(
            serde::de::Unexpected::TupleVariant,
            &"string",
        ))
    }
}
