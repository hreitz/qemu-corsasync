use std::ffi::{c_char, c_void};
use std::ptr::NonNull;

extern "C" {
    fn g_malloc(size: usize) -> *mut c_void;
}

/// Trait that allows converting string-like types into a C string in a buffer allocated with
/// `g_malloc()`, so qemu can safely `g_free()` that string.
pub trait ToGMallocCString {
    /// "lossy" because NUL bytes in the middle of the strings are ignored, i.e. will lead to the C
    /// string appearing truncated.
    fn to_gmalloc_cstring_lossy(&self) -> NonNull<c_char>;

    fn to_gmalloc_cstring(&self) -> Result<NonNull<c_char>, String>;
}

impl<S: AsRef<[u8]> + std::fmt::Debug> ToGMallocCString for S {
    fn to_gmalloc_cstring_lossy(&self) -> NonNull<c_char> {
        let source_slice: &[u8] = self.as_ref();
        let source_len = source_slice.len();
        let buf_len = source_len + 1;
        let cstring =
            unsafe { std::slice::from_raw_parts_mut(g_malloc(buf_len) as *mut u8, buf_len) };
        cstring[..source_len].copy_from_slice(source_slice);
        cstring[source_len] = 0;

        unsafe { NonNull::new_unchecked(cstring.as_ptr() as *mut c_char) }
    }

    fn to_gmalloc_cstring(&self) -> Result<NonNull<c_char>, String> {
        let source_slice: &[u8] = self.as_ref();
        // TODO: Is there a better way for `memchr()`?
        for b in source_slice {
            if *b == 0 {
                return Err(format!(
                    "String {:?} contains NUL character, cannot convert to C",
                    self
                ));
            }
        }

        let source_len = source_slice.len();
        let buf_len = source_len + 1;
        let cstring =
            unsafe { std::slice::from_raw_parts_mut(g_malloc(buf_len) as *mut u8, buf_len) };
        cstring[..source_len].copy_from_slice(source_slice);
        cstring[source_len] = 0;

        Ok(unsafe { NonNull::new_unchecked(cstring.as_ptr() as *mut c_char) })
    }
}
