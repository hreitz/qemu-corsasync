use std::ffi::{c_int, c_void};
use std::io;
use std::os::fd::AsRawFd;
use std::sync::Mutex;
use std::task::{Context, Poll, Waker};

pub struct AsyncFd<F: AsRawFd> {
    inner: F,
    ctx: *const (),
    state: Box<Mutex<AsyncFdState>>,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Interest(usize);

#[derive(Default)]
pub struct AsyncFdState {
    read_ready: bool,
    read_waiters: Vec<Waker>,

    write_ready: bool,
    write_waiters: Vec<Waker>,
}

pub struct AsyncFdReadReadyGuard<'a, F: AsRawFd> {
    wrapped: &'a AsyncFd<F>,
}

pub struct AsyncFdWriteReadyGuard<'a, F: AsRawFd> {
    wrapped: &'a AsyncFd<F>,
}

type FDHandler = extern "C" fn(*mut c_void);
type FDHandlerBool = extern "C" fn(*mut c_void) -> bool;

extern "C" {
    fn aio_set_fd_handler(
        ctx: *const (),
        fd: c_int,
        io_read: Option<FDHandler>,
        io_write: Option<FDHandler>,
        io_poll: Option<FDHandlerBool>,
        io_poll_ready: Option<FDHandler>,
        opaque: *mut c_void,
    );

    fn qemu_get_aio_context() -> *const ();
    fn qemu_get_current_aio_context() -> *const ();
}

impl<F: AsRawFd> AsyncFd<F> {
    /// Always succeeds, but return a `Result` to be compatible with tokio.
    pub fn new(inner: F) -> io::Result<Self> {
        Self::with_interest(inner, Interest::READABLE | Interest::WRITABLE)
    }

    pub fn with_interest(inner: F, interest: Interest) -> io::Result<Self> {
        let ctx = match unsafe { qemu_get_current_aio_context() } {
            ctx if !ctx.is_null() => ctx,
            _ => unsafe { qemu_get_aio_context() },
        };
        let state = Box::new(Mutex::new(AsyncFdState::default()));
        let state_ptr: *const Mutex<AsyncFdState> = &*state;
        unsafe {
            aio_set_fd_handler(
                ctx,
                inner.as_raw_fd(),
                interest.is_readable().then_some(async_fd_read as FDHandler),
                interest
                    .is_writable()
                    .then_some(async_fd_write as FDHandler),
                None,
                None,
                state_ptr as *mut c_void,
            )
        };

        Ok(AsyncFd { inner, ctx, state })
    }

    pub fn poll_read_ready<'a>(
        &'a self,
        cx: &Context<'_>,
    ) -> Poll<io::Result<AsyncFdReadReadyGuard<'a, F>>> {
        let mut state = self.state.lock().unwrap();
        if state.read_ready {
            Poll::Ready(Ok(AsyncFdReadReadyGuard { wrapped: self }))
        } else {
            state.read_waiters.push(cx.waker().clone());
            Poll::Pending
        }
    }

    pub fn poll_write_ready<'a>(
        &'a self,
        cx: &Context<'_>,
    ) -> Poll<io::Result<AsyncFdWriteReadyGuard<'a, F>>> {
        let mut state = self.state.lock().unwrap();
        if state.write_ready {
            Poll::Ready(Ok(AsyncFdWriteReadyGuard { wrapped: self }))
        } else {
            state.write_waiters.push(cx.waker().clone());
            Poll::Pending
        }
    }
}

extern "C" fn async_fd_read(opaque: *mut c_void) {
    let state: &Mutex<AsyncFdState> = unsafe { &*(opaque as *const Mutex<AsyncFdState>) };
    let read_waiters = {
        let mut state = state.lock().unwrap();
        state.read_ready = true;
        std::mem::take(&mut state.read_waiters)
    };
    for waker in read_waiters {
        waker.wake();
    }
}

extern "C" fn async_fd_write(opaque: *mut c_void) {
    let state: &Mutex<AsyncFdState> = unsafe { &*(opaque as *const Mutex<AsyncFdState>) };
    let write_waiters = {
        let mut state = state.lock().unwrap();
        state.write_ready = true;
        std::mem::take(&mut state.write_waiters)
    };
    for waker in write_waiters {
        waker.wake();
    }
}

impl<F: AsRawFd> Drop for AsyncFd<F> {
    fn drop(&mut self) {
        unsafe {
            aio_set_fd_handler(
                self.ctx,
                self.inner.as_raw_fd(),
                None,
                None,
                None,
                None,
                std::ptr::null_mut(),
            )
        };
    }
}

impl<F: AsRawFd> AsyncFdReadReadyGuard<'_, F> {
    pub fn clear_ready(&mut self) {
        self.wrapped.state.lock().unwrap().read_ready = false;
    }
}

impl<F: AsRawFd> AsyncFdWriteReadyGuard<'_, F> {
    pub fn clear_ready(&mut self) {
        self.wrapped.state.lock().unwrap().write_ready = false;
    }
}

impl Interest {
    pub const READABLE: Self = Interest(1 << 0);
    pub const WRITABLE: Self = Interest(1 << 1);

    pub const fn is_readable(self) -> bool {
        self.0 & Self::READABLE.0 != 0
    }

    pub const fn is_writable(self) -> bool {
        self.0 & Self::WRITABLE.0 != 0
    }

    pub const fn add(self, other: Self) -> Self {
        Interest(self.0 | other.0)
    }

    pub const fn remove(self, other: Self) -> Option<Self> {
        match self.0 & !other.0 {
            0 => None,
            x => Some(Interest(x)),
        }
    }
}

impl std::ops::BitOr for Interest {
    type Output = Self;

    fn bitor(self, other: Self) -> Self::Output {
        self.add(other)
    }
}
