use crate::ToGMallocCString;
use std::ffi::{c_char, c_int, c_void, CStr};

extern "C" {
    fn g_malloc0(size: usize) -> *mut c_void;
    fn error_free(err: *mut QemuError);

    static error_abort: *mut QemuError;
    static error_fatal: *mut QemuError;
    static error_warn: *mut QemuError;
}

#[repr(C)]
pub struct QemuError {
    msg: *mut c_char,
    err_class: QemuErrorClass,
    src: *const c_char,
    func: *const c_char,
    line: c_int,
    hint: *mut GString,
}

#[repr(C)]
struct GString {
    string: *mut c_char,
    len: usize,
    allocated_len: usize,
}

#[repr(C)]
#[allow(dead_code)]
enum QemuErrorClass {
    GenericError = 0,
    CommandNotFound = 1,
    DeviceNotActive = 2,
    DeviceNotFound = 3,
    KVMMissingCap = 4,
}

fn set_errp<E: std::error::Error>(errp: *mut *mut QemuError, error: E) {
    if errp.is_null() {
        // Do nothing
    } else if std::ptr::eq(errp, unsafe { std::ptr::addr_of!(error_abort) }) {
        panic!("Unexpected error: {}", error);
    } else if std::ptr::eq(errp, unsafe { std::ptr::addr_of!(error_fatal) }) {
        eprintln!("{}", error);
        std::process::exit(1);
    } else if std::ptr::eq(errp, unsafe { std::ptr::addr_of!(error_warn) }) {
        eprintln!("Warning: {}", error);
    } else {
        let heap_err = unsafe { g_malloc0(std::mem::size_of::<QemuError>()) } as *mut QemuError;
        let heap_err: &mut QemuError = unsafe {
            *errp = heap_err;
            &mut *heap_err
        };

        heap_err.msg = error.to_string().to_gmalloc_cstring_lossy().as_ptr();
        heap_err.err_class = QemuErrorClass::GenericError;
        heap_err.src = file!().as_ptr() as *const c_char;
        heap_err.func = "<undefined>".as_ptr() as *const c_char;
        heap_err.line = line!() as _;
    }
}

/// Convert from Rust's `Result` style to qemu's `errp` style: Run the given function, setting
/// `*errp` on error.
///
/// Return some result on success, and `None` on error, leaving it to the caller to decide what
/// value to return on error.
pub fn with_errp<R, E: std::error::Error, F: FnOnce() -> Result<R, E>>(
    f: F,
    errp: *mut *mut QemuError,
) -> Option<R> {
    match f() {
        Ok(result) => Some(result),
        Err(err) => {
            set_errp(errp, err);
            None
        }
    }
}

/// Same as `with_errp()`, but for the special case of I/O results and returning plain integers
/// using the convention of "0 on success, -errno on error".
pub fn with_errp_to_int<F: FnOnce() -> std::io::Result<()>>(
    f: F,
    errp: *mut *mut QemuError,
) -> c_int {
    match f() {
        Ok(()) => 0,
        Err(err) => {
            let result = -(err.raw_os_error().unwrap_or(libc::EIO) as c_int);
            set_errp(errp, err);
            result
        }
    }
}

/// Provide an `errp` variable to a closure to run a qemu-provided function that will return its
/// error in there, and translate the result into `Result<T, String>`.
pub fn from_errp<T, F: FnOnce(*mut *mut QemuError) -> T>(f: F) -> Result<T, String> {
    let mut local_err: *mut QemuError = std::ptr::null_mut();
    let r: T = f(&mut local_err as *mut *mut QemuError);
    if local_err.is_null() {
        Ok(r)
    } else {
        // qemu functions guarantee this is safe
        let err = unsafe { &*local_err };
        // Same for this
        let err_msg_cstr = unsafe { CStr::from_ptr(err.msg) };
        let err_msg = err_msg_cstr.to_string_lossy().to_string();

        // Safe: qemu has created this error, it should be able to free it
        unsafe { error_free(local_err) };

        Err(err_msg)
    }
}
