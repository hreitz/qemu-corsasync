#[cfg(not(feature = "tokio"))]
use std::ffi::c_void;
use std::future::Future;
use std::mem::ManuallyDrop;
#[cfg(not(feature = "tokio"))]
use std::pin::Pin;
#[cfg(not(feature = "tokio"))]
use std::sync::atomic::AtomicU32;
use std::sync::atomic::{AtomicU8, Ordering};
use std::sync::Arc;
#[cfg(not(feature = "tokio"))]
use std::sync::Mutex;
use std::task::{Context, Poll, RawWaker, RawWakerVTable, Waker};

extern "C" {
    fn qemu_in_coroutine() -> bool;
    fn qemu_coroutine_self() -> *const ();
    fn qemu_coroutine_yield();
    fn aio_co_wake(coroutine: *const ());
}

#[cfg(not(feature = "tokio"))]
extern "C" {
    fn aio_co_enter(context: *const (), coroutine: *const ());
    fn aio_poll(context: *const ());
    fn aio_wait_kick();
    fn qemu_coroutine_create(entry: extern "C" fn(*mut c_void), opaque: *mut c_void) -> *const ();
    fn qemu_get_current_aio_context() -> *const ();

    static global_aio_wait: AtomicU32;
}

#[repr(u8)]
enum CoWakerInnerState {
    Executing = 0,
    Yielding = 1,
    WakeSkipped = 2,
    Woken = 3,
}

struct CoWakerInner {
    co: *const (),
    state: AtomicU8,
}

// The co pointer is safe to send across threads
unsafe impl Send for CoWakerInner {}
unsafe impl Sync for CoWakerInner {}

fn co_waker_wake(co_ptr: *const ()) {
    let co = ManuallyDrop::new(unsafe { Arc::from_raw(co_ptr as *const CoWakerInner) });

    loop {
        match co.state.load(Ordering::Relaxed) {
            state if state == CoWakerInnerState::Executing as u8 => {
                if co
                    .state
                    .compare_exchange(
                        CoWakerInnerState::Executing as u8,
                        CoWakerInnerState::WakeSkipped as u8,
                        Ordering::Relaxed,
                        Ordering::Relaxed,
                    )
                    .is_ok()
                {
                    return;
                }
            }
            state if state == CoWakerInnerState::Yielding as u8 => {
                if co
                    .state
                    .compare_exchange(
                        CoWakerInnerState::Yielding as u8,
                        CoWakerInnerState::Woken as u8,
                        Ordering::Relaxed,
                        Ordering::Relaxed,
                    )
                    .is_ok()
                {
                    unsafe { aio_co_wake(co.co) };
                    return;
                }
            }
            state if state == CoWakerInnerState::WakeSkipped as u8 => return,
            state if state == CoWakerInnerState::Woken as u8 => return,
            _ => unreachable!(),
        }
    }
}

/// Basis for a `Waker` to wake a qemu coroutine.  The `RawWaker`'s data must be a pointer to the
/// `Coroutine`.
const CO_RAW_WAKER_VTABLE: RawWakerVTable = RawWakerVTable::new(
    // Clone: Copy the co pointer
    |co| {
        let co = ManuallyDrop::new(unsafe { Arc::from_raw(co as *const CoWakerInner) });
        RawWaker::new(
            Arc::into_raw(Arc::clone(&*co)) as *const (),
            &CO_RAW_WAKER_VTABLE,
        )
    },
    // Wake: Wake the coroutine
    co_waker_wake,
    // Wake by ref: Wake the coroutine
    co_waker_wake,
    // Drop: Drop reference to `co`
    |co| {
        let _ = unsafe { Arc::from_raw(co as *const CoWakerInner) };
    },
);

/// Run the given future to completion using qemu's coroutine functions.  When the future blocks,
/// the coroutine yields, and when the waker is woken, the coroutine is resumed.
///
/// Must only be called in qemu's coroutine context.
pub fn co_run_async<F: Future>(fut: F) -> F::Output {
    // qemu_in_coroutine() is always safe to call
    debug_assert!(unsafe { qemu_in_coroutine() });

    let co = Arc::new(CoWakerInner {
        // Safe if we're in a coroutine (guaranteed by caller, checked by debug_assert above)
        co: unsafe { qemu_coroutine_self() },
        state: AtomicU8::new(CoWakerInnerState::Executing as u8),
    });

    let raw_waker = RawWaker::new(
        Arc::into_raw(Arc::clone(&co)) as *const (),
        &CO_RAW_WAKER_VTABLE,
    );
    // Safe because the `RawWaker`'s data fits the vtable (`Arc<CoWakerInner>` as a pointer)
    let waker = unsafe { Waker::from_raw(raw_waker) };

    let mut context = Context::from_waker(&waker);

    futures::pin_mut!(fut);
    loop {
        if let Poll::Ready(result) = Future::poll(fut.as_mut(), &mut context) {
            return result;
        }

        if co
            .state
            .compare_exchange(
                CoWakerInnerState::Executing as u8,
                CoWakerInnerState::Yielding as u8,
                Ordering::Relaxed,
                Ordering::Relaxed,
            )
            .is_ok()
        {
            // Safe in coroutine context; guaranteed by the caller, and already required by
            // `coroutine_waker()`
            unsafe { qemu_coroutine_yield() };
        }

        co.state
            .store(CoWakerInnerState::Executing as u8, Ordering::Relaxed);
    }
}

/// If in a qemu coroutine context, run `co_run_async()`.  Otherwise, create a new async context,
/// run the future there, and wait until it is done.
pub fn co_run_async_or_block<F: Future>(fut: F) -> F::Output {
    if unsafe { qemu_in_coroutine() } {
        co_run_async(fut)
    } else {
        #[cfg(feature = "tokio")]
        {
            let rt = tokio::runtime::Runtime::new().unwrap();
            rt.block_on(fut)
        }

        #[cfg(not(feature = "tokio"))]
        {
            let result: Mutex<Option<F::Output>> = Mutex::new(None);
            let wrap_fut: Pin<Box<dyn Future<Output = ()>>> = Box::pin(async {
                *result.lock().unwrap() = Some(fut.await);
            });
            let boxed_fut = Box::new(wrap_fut);
            // Safe: Opaque type matches what the function expects
            let co = unsafe {
                qemu_coroutine_create(co_context, Box::into_raw(boxed_fut) as *mut c_void)
            };
            // Safe: Always safe to call
            let ctx = unsafe { qemu_get_current_aio_context() };
            // Safe: Parameters are correct
            unsafe { aio_co_enter(ctx, co) };

            // Implementation of qemu's `AIO_WAIT_WHILE`
            // Safe: `global_aio_wait` is an `unsigned` and accessed via atomics, which translates
            // into an `AtomicU32` in Rust
            unsafe { global_aio_wait.fetch_add(1, Ordering::SeqCst) };
            let r = loop {
                if let Some(r) = result.lock().unwrap().take() {
                    break r;
                }
                // Safe: `co_context()` will call `aio_wait_kick()` to wake us, and we have
                // incremented `global_aio_wait`, so that will wake us
                unsafe { aio_poll(ctx) };
            };
            // Safe: As `fetch_add` above
            unsafe { global_aio_wait.fetch_sub(1, Ordering::Relaxed) };

            r
        }
    }
}

#[no_mangle]
#[cfg(not(feature = "tokio"))]
extern "C" fn co_context(opaque: *mut c_void) {
    // Caller must ensure this type is correct and it was created with `Box::into_raw()`
    let fut = unsafe { Box::from_raw(opaque as *mut Pin<Box<dyn Future<Output = ()>>>) };
    co_run_async(*fut);
    // Safe: Always safe to call
    unsafe { aio_wait_kick() };
}
